FLAGS=-felf64 -o 

.PHONY: package clean main

package: main create_dir

raw_clean:
	rm -rf *.o main

main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

main.o: main.asm
	nasm $(FLAGS) main.o main.asm

dict.o: dict.asm
	nasm $(FLAGS) dict.o dict.asm

lib.o: lib.asm
	nasm $(FLAGS) lib.o lib.asm

create_dir:
	mkdir target
	mv *.o target
	mkdir src
	mv *.asm *.inc src

clean:
	rm -rf target main
	mv src/*.asm src/*.inc .
	rmdir src
	
