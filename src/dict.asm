%include "lib.inc"

global find_word

section .text

find_word:
.loop:
	cmp rsi, 0
	jz .error

	add rsi, 8
	push rdi
	push rsi
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 0
	jnz .found
	mov rsi, [rsi - 8]
	jmp .loop

.error:
	mov rsi, 8

.found:
	sub rsi, 8
	mov rax, rsi
	ret

