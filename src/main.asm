%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 256

section .data

too_long_string_error: db "String is too long.", 0
invalid_word_error: db "Invalid key responsed.", 0
input_prompt: db "Input key: ", 0
print_key: db "Value is ", 0

section .text

extern find_word

global _start
_start:
	mov rdi, input_prompt
	call print_string
	
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	mov rsi, BUFFER_SIZE
	call read_word
	cmp rax, 0
	mov rdi, too_long_string_error
	
	je .return
	
	mov rsi, LAST_LABEL
	mov rdi, rsp
	call find_word
	cmp rax, 0
	mov rdi, invalid_word_error
	je .return
	
	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	add rdi, 1       
	
	push rdi
	mov rdi, print_key
	call print_string
	pop rdi
	jmp .return
	
.return:
	add rsp, BUFFER_SIZE
	call print_string
	call print_newline
	call exit

