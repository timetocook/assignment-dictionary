global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global string_copy
global read_char
global read_word
global parse_uint
global parse_int

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    mov rdi, 0
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
.loop:
	cmp byte [rdi+rax], 0
	je .break
	inc rax
	jmp .loop
.break:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi 
    mov rdx, rax 
    mov rdi, 1   
    mov rax, 1   
    syscall      
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi      
    mov rsi, rsp 
    mov rdx, 1   
    mov rdi, 1   
    mov rax, 1   
    syscall      
    pop r8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r9
    push r10
    mov r10, rsp
    mov rax, rdi
    mov r9, 10
    dec rsp
.loop:
	xor rdx, rdx
	div r9
	add rdx, '0'
	dec rsp
	mov [rsp], dl
	cmp rax, 0
	jnz .loop
	mov rdi, rsp
	call print_string
    mov rsp, r10
    pop r10
    pop r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rbx
    mov rbx, rdi
    cmp rbx, 0
    jns .print
    mov rdi, '-'
    call print_char
    mov rdi, rbx
    neg rdi
.print:
	call print_uint
	pop rbx
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    mov rdi, [rsp]
    push rax
    call string_length
    cmp rax, [rsp]
    je .check
    add rsp, 3*8
.error:
    mov rax, 0
    ret

.check:
    pop rcx
    pop rsi
    pop rdi
.loop:
    cmp rcx, 0
    jbe .end
    mov dl, byte [rsi + rcx - 1]
    cmp dl, byte [rdi + rcx - 1]
    jne .error
    dec rcx
    jmp .loop

.end:
    mov rax, 1
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
	mov rsi, rsp
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1 
	syscall
	pop rax
	ret
	


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push rdi       
	push r9
	mov r9, rdi 
	push r10
	mov r10, rsi
	
.read_whitespace:
	call read_char
	cmp rax, 0x20
	je .read_whitespace
	cmp rax, 0x9
	je .read_whitespace
	cmp rax, 0xA
	je .read_whitespace
	cmp rax, 0
	je .end
	
.loop:
	cmp rax, 0x0
	je .return
	cmp rax, 0x20
	je .return
	cmp rax, 0x9
	je .return
	cmp rax, 0xA
	je .return
	dec r10       
	cmp r10, 0
	jbe .end
	mov byte [r9], al
	inc r9
	call read_char
	jmp .loop

.return:
	mov byte [r9], 0   
	pop r10             
	pop r9             
	mov rdi, [rsp]      
	call string_length
	mov rdx, rax        
	pop rax             
	ret

.end:
	pop r10             
	pop r9
	pop rdi
	mov rax, 0
	mov rdx, 0
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
 xor rcx, rcx
 mov r8, 10

.loop:
 movzx r9, byte[rdi+rcx]
 cmp r9b, '0'
 jb .end
 cmp r9b, '9'
 ja .end
 xor rdx,rdx
 mul r8
 and r9b, 0x0f
 add rax, r9
 inc rcx
 jmp .loop

.end:
 mov rdx, rcx
 ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	cmp byte [rdi], '-'
	je .neg
	jmp parse_uint
	ret 
.neg:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    call string_length
	cmp rax, rdx
	jge .return
	mov rcx, 0
.loop:
	mov r10, [rdi+rcx]
	mov [rsi+rcx], r10
	cmp rcx, rax
	je .break
	inc rcx
	jmp .loop
.return:
	mov rax, 0
.break:
	ret

